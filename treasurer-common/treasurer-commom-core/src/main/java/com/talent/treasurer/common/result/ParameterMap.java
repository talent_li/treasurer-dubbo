package com.talent.treasurer.common.result;



import com.talent.treasurer.common.util.StringTools;

import java.util.HashMap;

/***

 * @DESCRIPTION ${DESCRIPTION}
 *@className com.zien.system.result
 * @author Talent
 * @create 2017-05-19 上午10:43
 */
public class ParameterMap extends HashMap<String, Object> {
   private final static Integer pageSize = 20;
    private static final long serialVersionUID = 1L;

   public  static ParameterMap setPage(Integer page){
       if(StringTools.isNullOrEmpty(page)){
           page = 1;
       }
       if(page<1){
           page=1;
       }
       ParameterMap parameterMap = new ParameterMap();

       page = page * pageSize- pageSize;
       parameterMap.put("page",page);
       parameterMap.put("pageSize",pageSize);
       return  parameterMap;
   }

    public ParameterMap put(String key, Object value) {
        super.put(key, value);
        return this;
    }

}
