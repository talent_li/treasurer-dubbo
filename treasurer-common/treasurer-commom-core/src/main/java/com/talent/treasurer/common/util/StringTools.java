package com.talent.treasurer.common.util;

import java.util.Random;

/***

 * @DESCRIPTION 字符串工具类
 *@className com.zien.system.util
 * @author Talent
 * @create 2017-03-22 下午6:00
 */
public class StringTools {

    /**
     * 如果是null返回true，如果是String类型则还会判断是否是""(空字符串)，如果是""返回true，否则返回FALSE
     *
     * @param obj
     * @return
     */
    public static boolean isNullOrEmpty(Object obj) {
        return null == obj ? true : (obj instanceof String && "".equals(obj.toString().trim())) ? true : false;
    }

    /**
     * 检查请求参数必填字段，如果有一个为空则返回TRUE;
     *
     * @param
     * @param params 参数数组
     * @return
     */
    public static boolean checkParamIsNull(Object[] params) {
        for (int i = 0; i < params.length; i++) {
            if (isNullOrEmpty(params[i])) {
                return true;
            }

        }
        return false;
    }

    /**
     * 产生随机的六位数
     *
     * @return
     */
    public static String getSix() {
        Random rad = new Random();

        String result = rad.nextInt(1000000) + "";

        if (result.length() != 6) {
            return getSix();
        }
        return result;
    }

    public static String getNumber(Long number) {
        if (number < 10) {
            return "00" + number;
        } else if (number < 100) {
            return "0" + number;
        } else {
            return "" + number;
        }
    }


    public static Long[] getLongArrayByStringArray(String[] strings) {

        Long[] longs = new Long[strings.length];
        for (int i = 0; i < strings.length; i++) {
            longs[i] = Long.valueOf(strings[i]);
        }

        return longs;
    }
}
