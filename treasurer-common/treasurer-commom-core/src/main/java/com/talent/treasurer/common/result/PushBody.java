package com.talent.treasurer.common.result;


import com.talent.treasurer.common.util.StringTools;

/***

 * @DESCRIPTION ${DESCRIPTION}
 *@className com.zien.system.result
 * @author Talent
 * @create 2017-04-16 下午3:26
 */
public class PushBody {
    private String type;
    private String content;
    /**
     * 设备类型
     */
    private String client;


    public PushBody(){

    }


    public PushBody(String client){

        if(StringTools.isNullOrEmpty(client)){
            client = "iPad";
        }
        this.client = client;

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }
}
