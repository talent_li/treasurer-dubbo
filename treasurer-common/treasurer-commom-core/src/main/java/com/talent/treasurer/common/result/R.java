package com.talent.treasurer.common.result;

import java.util.HashMap;
import java.util.Map;

/***

 * @返回数据类
 *@className com.zien.system.result
 * @author Talent
 * @create 2017-03-17 上午10:55
 */
public class R extends HashMap<String, Object> {
    private static final long serialVersionUID = 1L;

    public R() {
        put("code", 200);
    }

    public static R error() {
        return error(500, "未知异常，请联系管理员");
    }

    public static R error(String msg) {
        return error(500, msg);
    }

    public static R error(int code, String msg) {
        R r = new R();
        r.put("code", code);
        r.put("msg", msg);
        return r;
    }

    public static R paramError() {
        return error(100, "请求参数错误");
    }

    public static R verifyError() {
        return error(101, "请重新登录");
    }

    public static R ok(String msg) {
        R r = new R();
        r.put("msg", msg);
        return r;
    }

    public static R ok(Map<String, Object> map) {
        R r = new R();
        r.putAll(map);
        return r;
    }

    public static R ok() {
        return new R();
    }

    public R put(String key, Object value) {
        super.put(key, value);
        return this;
    }

    public R putData(Object value) {
//        if(StringTools.isNullOrEmpty(value)){
//            super.put("code", 103);
//            super.put("msg", "没有更多数据");
//            return this;
//        }
        return put("data", value);
    }
}

