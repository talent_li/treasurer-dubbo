/*
Navicat MySQL Data Transfer

Source Server         : macbook
Source Server Version : 50717
Source Host           : 192.168.123.221:3306
Source Database       : treasurer

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2018-03-04 09:50:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for company
-- ----------------------------
DROP TABLE IF EXISTS `company`;
CREATE TABLE `company` (
  `company_id` bigint(30) NOT NULL COMMENT '主键',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '公司名称',
  `password` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '地址',
  `phone` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '手机号',
  `region` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_num` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '服务业务员编号',
  `accredit_pad_number` tinyint(4) DEFAULT '1' COMMENT '授权ipad的数量',
  `accredit_phone_number` tinyint(4) DEFAULT '1' COMMENT '手机授权',
  `gmt_service` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '最后使用时间',
  `authentication_status` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT '0' COMMENT '认证状态',
  `authentication_user_id` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '认证用户',
  `openid` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wechat_accredit_number` tinyint(4) DEFAULT '1',
  `status` tinyint(4) DEFAULT '0' COMMENT '用户状态 ',
  `gmt_create` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gmt_modified` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_delete` tinyint(4) DEFAULT '0' COMMENT '是否已经删除',
  PRIMARY KEY (`company_id`),
  KEY `gmt_create` (`gmt_create`),
  KEY `is_delete` (`is_delete`),
  KEY `status` (`status`),
  KEY `status_2` (`status`,`gmt_create`,`is_delete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for shop
-- ----------------------------
DROP TABLE IF EXISTS `shop`;
CREATE TABLE `shop` (
  `shop_id` bigint(20) NOT NULL COMMENT '主键',
  `company_id` bigint(20) DEFAULT NULL,
  `shop_name` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '店铺名',
  `phone` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '地址',
  `create_time` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建时间',
  `status` tinyint(4) DEFAULT '1' COMMENT '0为禁用，1为可用',
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_shop_id` bigint(20) DEFAULT '0' COMMENT '父级店铺id',
  `shop_type` tinyint(10) DEFAULT '1' COMMENT '店铺类型1为共享商品店铺，2为独立商品店铺',
  `gmt_create` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gmt_modified` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_delete` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`shop_id`),
  KEY `company_id` (`company_id`),
  KEY `shop_id` (`shop_id`),
  KEY `is_delete` (`is_delete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` bigint(30) NOT NULL,
  `company_id` bigint(30) DEFAULT NULL COMMENT '公司id',
  `img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '登录名',
  `password` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '密码',
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '手机号码',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态',
  `number` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '编号',
  `boss` int(4) DEFAULT '0' COMMENT '1为老板，2为普通用户',
  `position` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '职位',
  `we_chat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '微信号',
  `ID_card` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '身份证号',
  `print_content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '打印内容',
  `token` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '登录token',
  `permission` text COLLATE utf8mb4_unicode_ci COMMENT '权限',
  `gmt_create` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建时间',
  `gmt_last_login` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '最后登录时间',
  `gmt_modified` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '修改时间',
  `is_delete` tinyint(4) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`user_id`),
  KEY `company_id` (`company_id`),
  KEY `company_id_2` (`company_id`,`is_delete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for user_shop
-- ----------------------------
DROP TABLE IF EXISTS `user_shop`;
CREATE TABLE `user_shop` (
  `user_shop_id` bigint(30) NOT NULL,
  `user_id` bigint(30) DEFAULT NULL COMMENT '用户id',
  `shop_id` bigint(30) DEFAULT NULL COMMENT '店铺id',
  `gmt_create` varchar(30) DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` varchar(30) DEFAULT NULL COMMENT '修改时间',
  `is_delete` int(11) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`user_shop_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
