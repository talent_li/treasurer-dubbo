package com.talent.treasurer.user.api.modules.service.impl;

import com.talent.treasurer.user.api.modules.entity.Company;
import com.talent.treasurer.user.api.modules.mapper.CompanyMapper;
import com.talent.treasurer.user.api.modules.service.ICompanyService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author talent
 * @since 2018-03-02
 */
@Service
@Transactional
public class CompanyServiceImpl extends ServiceImpl<CompanyMapper, Company> implements ICompanyService {
	
}
