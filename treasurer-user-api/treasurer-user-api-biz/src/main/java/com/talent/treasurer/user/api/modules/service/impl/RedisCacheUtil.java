package com.talent.treasurer.user.api.modules.service.impl;

/***

 * @DESCRIPTION ${DESCRIPTION}
 *@className com.zien.system.util
 * @author Talent
 * @create 2017-03-17 上午11:32
 */

import com.talent.treasurer.user.api.modules.service.IRedisCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.Service;

import java.util.*;


@Service
public class RedisCacheUtil<T> implements IRedisCache<T> {

    static Logger logger = LoggerFactory.getLogger(RedisCacheUtil.class);
    @Autowired
    @Qualifier("redisTemplate")
    public RedisTemplate redisTemplate;

    /**
     * 缓存基本对象，Integer，String，实体类等
     *
     * @param key   key值
     * @param value 对象
     * @param <T>   对象类型
     * @return
     */
    public <T> void setCacheObject(String key, T value) {
        try {
            ValueOperations<String, T> operation = redisTemplate.opsForValue();

            operation.set(key, value);

        }catch (Exception e){
           logger.error(e.getMessage(),e);

        }
    }

    /**
     * 获取缓存基本对象
     *
     * @param key key值
     * @param <T> 返回对象
     * @return
     */
    public <T> T getCacheObject(String key) {
        try {
        ValueOperations<String, T> operations = redisTemplate.opsForValue();
        return operations.get(key);
        }catch (Exception e){
            logger.error(e.getMessage(),e);
            return null;
        }
    }


    /**
     * 缓存list数据
     *
     * @param key      缓存的键值
     * @param dataList 待缓存的List数据
     * @param <T>
     * @return
     */
    public <T> void setCacheList(String key, List<T> dataList) {
        try {
            ListOperations listOperations = redisTemplate.opsForList();

            if (null != dataList) {
                for (int i = 0; i < dataList.size(); i++) {
                    listOperations.rightPush(key, dataList.get(i));
                }
            }

        }catch (Exception e){
            logger.error(e.getMessage(),e);

        }
    }


    /**
     * 获得缓存的list对象
     *
     * @param key 缓存的键值
     * @param <T>
     * @return 缓存键值对应的数据
     */
    public <T> List<T> getCacheList(String key) {
        try{
            List<T> dataList = new ArrayList<T>();
            ListOperations<String, T> listOperation = redisTemplate.opsForList();
            Long size = listOperation.size(key);

            for (int i = 0; i < size; i++) {
                dataList.add((T) listOperation.leftPop(key));
            }

            return dataList;
        }catch (Exception e){
            logger.error(e.getMessage(),e);
            return null;
        }
    }


    /**
     * 缓存Set
     *
     * @param key     缓存键值
     * @param dataSet 缓存的数据
     * @return 缓存数据的对象
     */
    public <T> void setCacheSet(String key, Set<T> dataSet) {
        try{
        BoundSetOperations<String, T> setOperation = redisTemplate.boundSetOps(key);
        /*T[] t = (T[]) dataSet.toArray();
             setOperation.add(t);*/


        Iterator<T> it = dataSet.iterator();
        while (it.hasNext()) {
            setOperation.add(it.next());
        }


        }catch (Exception e){
            logger.error(e.getMessage(),e);

        }
    }

    /**
     * 获得缓存的set
     *
     * @param key
     * @param
     * @return
     */
    public Set<T> getCacheSet(String key) {
        Set<T> dataSet = new HashSet<T>();
        BoundSetOperations<String, T> operation = redisTemplate.boundSetOps(key);

        Long size = operation.size();
        for (int i = 0; i < size; i++) {
            dataSet.add(operation.pop());
        }
        return dataSet;
    }

    /**
     * 缓存Map
     *
     * @param key
     * @param dataMap
     * @return
     */
    public <T> void setCacheMap(String key, Map<String, T> dataMap) {

        HashOperations hashOperations = redisTemplate.opsForHash();
        if (null != dataMap) {

            for (Map.Entry<String, T> entry : dataMap.entrySet()) {

                /*System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());  */
                hashOperations.put(key, entry.getKey(), entry.getValue());
            }

        }


    }

    /**
     * 获得缓存的Map
     *
     * @param key
     * @return
     */
    public <T> Map<String, T> getCacheMap(String key) {
        Map<String, T> map = redisTemplate.opsForHash().entries(key);
        /*Map<String, T> map = hashOperation.entries(key);*/
        return map;
    }


    /**
     * 缓存Map
     *
     * @param key
     * @param dataMap
     * @return
     */
    public <T> void setCacheIntegerMap(String key, Map<Integer, T> dataMap) {
        HashOperations hashOperations = redisTemplate.opsForHash();
        if (null != dataMap) {

            for (Map.Entry<Integer, T> entry : dataMap.entrySet()) {

                /*System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());  */
                hashOperations.put(key, entry.getKey(), entry.getValue());
            }

        }


    }

    /**
     * 获得缓存的Map
     *
     * @param key
     * @return
     */
    public <T> Map<Integer, T> getCacheIntegerMap(String key) {
        Map<Integer, T> map = redisTemplate.opsForHash().entries(key);
        /*Map<String, T> map = hashOperation.entries(key);*/
        return map;
    }

    /**
     * 通过key，删除缓存
     *
     * @param key
     */
    public void removeCache(String key) {
        redisTemplate.delete(key);
    }


}
