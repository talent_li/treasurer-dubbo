package com.talent.treasurer.user.api.modules.datasource;

public enum DBTypeEnum {

    one("dataSource_read"), two("dataSource_write");
    private String value;

    DBTypeEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}