package com.talent.treasurer.user.api.modules.service.impl;

import com.talent.treasurer.user.api.modules.entity.UserShop;
import com.talent.treasurer.user.api.modules.mapper.UserShopMapper;
import com.talent.treasurer.user.api.modules.service.IUserShopService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author talent
 * @since 2018-03-02
 */
@Service
@Transactional
public class UserShopServiceImpl extends ServiceImpl<UserShopMapper, UserShop> implements IUserShopService {
	
}
