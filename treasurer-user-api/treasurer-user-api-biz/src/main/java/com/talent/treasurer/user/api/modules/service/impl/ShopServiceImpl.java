package com.talent.treasurer.user.api.modules.service.impl;

import com.talent.treasurer.user.api.modules.entity.Shop;
import com.talent.treasurer.user.api.modules.mapper.ShopMapper;
import com.talent.treasurer.user.api.modules.service.IShopService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author talent
 * @since 2018-03-02
 */
@Service
@Transactional
public class ShopServiceImpl extends ServiceImpl<ShopMapper, Shop> implements IShopService {
	
}
