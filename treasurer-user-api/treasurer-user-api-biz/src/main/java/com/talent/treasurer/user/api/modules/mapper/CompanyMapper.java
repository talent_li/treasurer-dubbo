package com.talent.treasurer.user.api.modules.mapper;

import com.talent.treasurer.user.api.modules.entity.Company;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author talent
 * @since 2018-03-02
 */
public interface CompanyMapper extends BaseMapper<Company> {

}