package com.talent.treasurer.user.api.test.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class DubboProviders {
	
	private static final Logger logger = LoggerFactory.getLogger(DubboProviders.class);
	
    private DubboProviders() { }

	public static void main(String[] args) {
		try {
			@SuppressWarnings("resource")
			ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring/applicationContext-spring.xml");
			context.start();
			logger.info("Dubbo started!");
		} catch (Exception e) {
			logger.error("== DubboProvider context start error:", e);
		}
		synchronized (DubboProviders.class) {
			while (true) {
				try {
					DubboProviders.class.wait();
				} catch (InterruptedException e) {
					logger.error("== synchronized error:", e);
				}
			}
		}
	}
}
