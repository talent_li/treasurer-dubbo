package com.talent.treasurer.user.api.modules.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author talent
 * @since 2018-03-02
 */
@Controller
@RequestMapping("/user/userShop")
public class UserShopController {
	
}
