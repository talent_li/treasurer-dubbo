package com.talent.treasurer.user.api.modules.controller;


import com.talent.treasurer.common.controller.BaseController;
import com.talent.treasurer.common.result.ResponseResult;
import com.talent.treasurer.common.util.StringTools;
import com.talent.treasurer.user.api.modules.entity.Company;
import com.talent.treasurer.user.api.modules.service.ICompanyService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author talent
 * @since 2018-03-02
 */
@Controller
@RequestMapping("/user/company")

public class CompanyController extends BaseController {

    Logger logger  = LoggerFactory.getLogger("CompanyController");

    @Autowired
    private ICompanyService companyServiceImpl;


    @RequestMapping("add")
    @ResponseBody
    public ResponseResult add(Company company){
        try {

            Object [] params = new Object[]{company.getName(), company.getPhone(),company.getPassword()};
            if(StringTools.checkParamIsNull(params)){
                return super.paramError();
            }

            companyServiceImpl.insert(company);
            return super.success(company);



        }catch (Exception e){
            logger.error(e.getMessage(), e);
            return  super.systemError();

        }

        }
}
