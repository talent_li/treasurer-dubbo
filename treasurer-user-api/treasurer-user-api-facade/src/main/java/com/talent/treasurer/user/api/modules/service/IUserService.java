package com.talent.treasurer.user.api.modules.service;

import com.talent.treasurer.user.api.modules.entity.User;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author talent
 * @since 2018-03-02
 */
public interface IUserService extends IService<User> {
	
}
