package com.talent.treasurer.user.api.modules.service;

import com.talent.treasurer.user.api.modules.entity.Company;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author talent
 * @since 2018-03-02
 */
public interface ICompanyService extends IService<Company> {
	
}
