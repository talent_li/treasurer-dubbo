package com.talent.treasurer.user.api.modules.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface IRedisCache<T> {
    /**
     * 缓存基本对象，Integer，String，实体类等
     *
     * @param key   key值
     * @param value 对象
     * @param <T>   对象类型
     * @return
     */
    public <T> void setCacheObject(String key, T value) ;

    /**
     * 获取缓存基本对象
     *
     * @param key key值
     * @param <T> 返回对象
     * @return
     */
    public <T> T getCacheObject(String key) ;


    /**
     * 缓存list数据
     *
     * @param key      缓存的键值
     * @param dataList 待缓存的List数据
     * @param <T>
     * @return
     */
    public <T> void setCacheList(String key, List<T> dataList) ;


    /**
     * 获得缓存的list对象
     *
     * @param key 缓存的键值
     * @param <T>
     * @return 缓存键值对应的数据
     */
    public <T> List<T> getCacheList(String key) ;


    /**
     * 缓存Set
     *
     * @param key     缓存键值
     * @param dataSet 缓存的数据
     * @return 缓存数据的对象
     */
    public <T> void setCacheSet(String key, Set<T> dataSet) ;

    /**
     * 获得缓存的set
     *
     * @param key
     * @param
     * @return
     */
    public Set<T> getCacheSet(String key) ;

    /**
     * 缓存Map
     *
     * @param key
     * @param dataMap
     * @return
     */
    public <T> void setCacheMap(String key, Map<String, T> dataMap) ;

    /**
     * 获得缓存的Map
     *
     * @param key
     * @return
     */
    public <T> Map<String, T> getCacheMap(String key);


    /**
     * 缓存Map
     *
     * @param key
     * @param dataMap
     * @return
     */
    public <T> void setCacheIntegerMap(String key, Map<Integer, T> dataMap);



    /**
     * 获得缓存的Map
     *
     * @param key
     * @return
     */
    public <T> Map<Integer, T> getCacheIntegerMap(String key) ;
    /**
     * 通过key，删除缓存
     *
     * @param key
     */
    public void removeCache(String key);

}
