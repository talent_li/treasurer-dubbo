package com.talent.treasurer.user.api.modules.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author talent
 * @since 2018-03-02
 */
public class Company extends Model<Company> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId("company_id")
	private Long companyId;
    /**
     * 公司名称
     */
	private String name;
	private String password;
    /**
     * 地址
     */
	private String address;
    /**
     * 手机号
     */
	private String phone;
	private String region;
    /**
     * 服务业务员编号
     */
	@TableField("service_num")
	private String serviceNum;
    /**
     * 授权ipad的数量
     */
	@TableField("accredit_pad_number")
	private Integer accreditPadNumber;
    /**
     * 手机授权
     */
	@TableField("accredit_phone_number")
	private Integer accreditPhoneNumber;
    /**
     * 最后使用时间
     */
	@TableField("gmt_service")
	private String gmtService;
    /**
     * 认证状态
     */
	@TableField("authentication_status")
	private String authenticationStatus;
    /**
     * 认证用户
     */
	@TableField("authentication_user_id")
	private String authenticationUserId;
	private String openid;
	@TableField("wechat_accredit_number")
	private Integer wechatAccreditNumber;
    /**
     * 用户状态 
     */
	private Integer status;
	@TableField("gmt_create")
	private String gmtCreate;
	@TableField("gmt_modified")
	private String gmtModified;
    /**
     * 是否已经删除
     */
	@TableField("is_delete")
	private Integer isDelete;


	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getServiceNum() {
		return serviceNum;
	}

	public void setServiceNum(String serviceNum) {
		this.serviceNum = serviceNum;
	}

	public Integer getAccreditPadNumber() {
		return accreditPadNumber;
	}

	public void setAccreditPadNumber(Integer accreditPadNumber) {
		this.accreditPadNumber = accreditPadNumber;
	}

	public Integer getAccreditPhoneNumber() {
		return accreditPhoneNumber;
	}

	public void setAccreditPhoneNumber(Integer accreditPhoneNumber) {
		this.accreditPhoneNumber = accreditPhoneNumber;
	}

	public String getGmtService() {
		return gmtService;
	}

	public void setGmtService(String gmtService) {
		this.gmtService = gmtService;
	}

	public String getAuthenticationStatus() {
		return authenticationStatus;
	}

	public void setAuthenticationStatus(String authenticationStatus) {
		this.authenticationStatus = authenticationStatus;
	}

	public String getAuthenticationUserId() {
		return authenticationUserId;
	}

	public void setAuthenticationUserId(String authenticationUserId) {
		this.authenticationUserId = authenticationUserId;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public Integer getWechatAccreditNumber() {
		return wechatAccreditNumber;
	}

	public void setWechatAccreditNumber(Integer wechatAccreditNumber) {
		this.wechatAccreditNumber = wechatAccreditNumber;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(String gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public String getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(String gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Integer getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}

	@Override
	protected Serializable pkVal() {
		return this.companyId;
	}

	@Override
	public String toString() {
		return "Company{" +
			"companyId=" + companyId +
			", name=" + name +
			", password=" + password +
			", address=" + address +
			", phone=" + phone +
			", region=" + region +
			", serviceNum=" + serviceNum +
			", accreditPadNumber=" + accreditPadNumber +
			", accreditPhoneNumber=" + accreditPhoneNumber +
			", gmtService=" + gmtService +
			", authenticationStatus=" + authenticationStatus +
			", authenticationUserId=" + authenticationUserId +
			", openid=" + openid +
			", wechatAccreditNumber=" + wechatAccreditNumber +
			", status=" + status +
			", gmtCreate=" + gmtCreate +
			", gmtModified=" + gmtModified +
			", isDelete=" + isDelete +
			"}";
	}
}
