package com.talent.treasurer.user.api.modules.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author talent
 * @since 2018-03-02
 */
public class Shop extends Model<Shop> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId("shop_id")
	private Long shopId;
	@TableField("company_id")
	private Long companyId;
    /**
     * 店铺名
     */
	@TableField("shop_name")
	private String shopName;
	private String phone;
    /**
     * 地址
     */
	private String address;
    /**
     * 创建时间
     */
	@TableField("create_time")
	private String createTime;
    /**
     * 0为禁用，1为可用
     */
	private Integer status;
	private String icon;
    /**
     * 父级店铺id
     */
	@TableField("parent_shop_id")
	private Long parentShopId;
    /**
     * 店铺类型1为共享商品店铺，2为独立商品店铺
     */
	@TableField("shop_type")
	private Integer shopType;
	@TableField("gmt_create")
	private String gmtCreate;
	@TableField("gmt_modified")
	private String gmtModified;
	@TableField("is_delete")
	private Integer isDelete;


	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Long getParentShopId() {
		return parentShopId;
	}

	public void setParentShopId(Long parentShopId) {
		this.parentShopId = parentShopId;
	}

	public Integer getShopType() {
		return shopType;
	}

	public void setShopType(Integer shopType) {
		this.shopType = shopType;
	}

	public String getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(String gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public String getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(String gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Integer getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}

	@Override
	protected Serializable pkVal() {
		return this.shopId;
	}

	@Override
	public String toString() {
		return "Shop{" +
			"shopId=" + shopId +
			", companyId=" + companyId +
			", shopName=" + shopName +
			", phone=" + phone +
			", address=" + address +
			", createTime=" + createTime +
			", status=" + status +
			", icon=" + icon +
			", parentShopId=" + parentShopId +
			", shopType=" + shopType +
			", gmtCreate=" + gmtCreate +
			", gmtModified=" + gmtModified +
			", isDelete=" + isDelete +
			"}";
	}
}
