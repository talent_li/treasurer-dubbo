package com.talent.treasurer.user.api.modules.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author talent
 * @since 2018-03-02
 */
public class User extends Model<User> {

    private static final long serialVersionUID = 1L;

    @TableId("user_id")
	private Long userId;
    /**
     * 公司id
     */
	@TableField("company_id")
	private Long companyId;
	private String img;
    /**
     * 登录名
     */
	private String name;
    /**
     * 密码
     */
	private String password;
    /**
     * 手机号码
     */
	private String phone;
    /**
     * 状态
     */
	private Integer status;
    /**
     * 编号
     */
	private String number;
    /**
     * 1为老板，2为普通用户
     */
	private Integer boss;
    /**
     * 职位
     */
	private String position;
    /**
     * 微信号
     */
	@TableField("we_chat")
	private String weChat;
    /**
     * 身份证号
     */
	@TableField("ID_card")
	private String IDCard;
    /**
     * 打印内容
     */
	@TableField("print_content")
	private String printContent;
    /**
     * 登录token
     */
	private String token;
    /**
     * 权限
     */
	private String permission;
    /**
     * 创建时间
     */
	@TableField("gmt_create")
	private String gmtCreate;
    /**
     * 最后登录时间
     */
	@TableField("gmt_last_login")
	private String gmtLastLogin;
    /**
     * 修改时间
     */
	@TableField("gmt_modified")
	private String gmtModified;
    /**
     * 是否删除
     */
	@TableField("is_delete")
	private Integer isDelete;


	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Integer getBoss() {
		return boss;
	}

	public void setBoss(Integer boss) {
		this.boss = boss;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getWeChat() {
		return weChat;
	}

	public void setWeChat(String weChat) {
		this.weChat = weChat;
	}

	public String getIDCard() {
		return IDCard;
	}

	public void setIDCard(String IDCard) {
		this.IDCard = IDCard;
	}

	public String getPrintContent() {
		return printContent;
	}

	public void setPrintContent(String printContent) {
		this.printContent = printContent;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	public String getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(String gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public String getGmtLastLogin() {
		return gmtLastLogin;
	}

	public void setGmtLastLogin(String gmtLastLogin) {
		this.gmtLastLogin = gmtLastLogin;
	}

	public String getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(String gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Integer getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}

	@Override
	protected Serializable pkVal() {
		return this.userId;
	}

	@Override
	public String toString() {
		return "User{" +
			"userId=" + userId +
			", companyId=" + companyId +
			", img=" + img +
			", name=" + name +
			", password=" + password +
			", phone=" + phone +
			", status=" + status +
			", number=" + number +
			", boss=" + boss +
			", position=" + position +
			", weChat=" + weChat +
			", IDCard=" + IDCard +
			", printContent=" + printContent +
			", token=" + token +
			", permission=" + permission +
			", gmtCreate=" + gmtCreate +
			", gmtLastLogin=" + gmtLastLogin +
			", gmtModified=" + gmtModified +
			", isDelete=" + isDelete +
			"}";
	}
}
