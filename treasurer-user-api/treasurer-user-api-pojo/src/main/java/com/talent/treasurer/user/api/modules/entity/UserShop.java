package com.talent.treasurer.user.api.modules.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author talent
 * @since 2018-03-02
 */
@TableName("user_shop")
public class UserShop extends Model<UserShop> {

    private static final long serialVersionUID = 1L;

    @TableId("user_shop_id")
	private Long userShopId;
    /**
     * 用户id
     */
	@TableField("user_id")
	private Long userId;
    /**
     * 店铺id
     */
	@TableField("shop_id")
	private Long shopId;
    /**
     * 创建时间
     */
	@TableField("gmt_create")
	private String gmtCreate;
    /**
     * 修改时间
     */
	@TableField("gmt_modified")
	private String gmtModified;
    /**
     * 是否删除
     */
	@TableField("is_delete")
	private Integer isDelete;


	public Long getUserShopId() {
		return userShopId;
	}

	public void setUserShopId(Long userShopId) {
		this.userShopId = userShopId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(String gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public String getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(String gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Integer getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}

	@Override
	protected Serializable pkVal() {
		return this.userShopId;
	}

	@Override
	public String toString() {
		return "UserShop{" +
			"userShopId=" + userShopId +
			", userId=" + userId +
			", shopId=" + shopId +
			", gmtCreate=" + gmtCreate +
			", gmtModified=" + gmtModified +
			", isDelete=" + isDelete +
			"}";
	}
}
